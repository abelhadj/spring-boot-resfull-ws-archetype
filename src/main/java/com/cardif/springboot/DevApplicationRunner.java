package com.cardif.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(DevApplicationRunner.class, args);
	}
}
